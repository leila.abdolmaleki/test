import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";

import Login from "./components/login.component";


function App() {
  return (<Router>
    <div className="App">
      <div className="auth-wrapper">
        <div className="auth-inner">
          <Login></Login>
        </div>
        <p className=" text-center text-white">ارتباط با مرکز تماس دانش بنیان : ۰۲۱۸۳۵۳۴</p>
      </div>
    </div></Router>
  );
}

export default App;
