import React, { Component } from "react";
import allah from './allah.png';




export default class Login extends Component {
    render() {
        return (<>
            <form className="was-validated">

                <img src={allah} className="float-right" />
                <h3 className="text-center" >ریاست جمهوری<br /> معاونت علمی و فناوری</h3>

                <p className="text-center">سامانه ارزیابی و تشخیص صلاحیت شرکت ها و موسسات دانش بنیان</p>
                <div className="form-group">

                    <input
                        style={{
                            boxSizing: "border-box",
                            border: "none",
                            borderBottom: "1px solid #ccc"
                        }}
                        type="email" className="form-control" placeholder="نام کاربری " required />
                    <div className="valid-feedback"></div>
                    <div className="invalid-feedback">لطفا متن صحیح را وارد کنید</div>
                </div>

                <div className="form-group">

                    <input style={{
                        boxSizing: "border-box",
                        border: "none",
                        borderBottom: "1px solid #ccc"
                    }} type="password" className="form-control" placeholder="رمز عبور" required />
                    <div className="valid-feedback"></div>
                    <div className="invalid-feedback">لطفا متن صحیح را وارد کنید</div>
                </div>

                <button type="submit" className="btn btn-secondary btn-block" ref="http://localhost:3001">ورود</button>
                <div className="text-center my-2">
                    <button type="submit" className="btn btn-light mx-sm-2"> فراموشی رمز</button>
                    <button type="submit" className="btn btn-light mx-sm-2">ثبت نام</button>
                </div>


            </form> </>
        );
    }
}



